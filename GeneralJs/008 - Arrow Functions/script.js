const multiply = () => 10 * 5;

console.log(multiply());

let add = function (x, y) {
	return x + y;
};

console.log(add(10, 20)); // 30


let add = (x, y) => x + y;

console.log(add(10, 20)); // 30;