const arr = ['John', 'Nick', 'Bob', 'Michael', 'Mary'];

// console.log(arr[0]);
// console.log(arr[1]);
// console.log(arr[2]);
// console.log(arr[3]);
// console.log(arr[4]);

// for (let i = 0; i < arr.length; i++) {
// 	// console.log(arr[i]);
// 	if (arr[i] === 'Bob') {
// 		console.log(arr[i] + ' is my brother');
// 		continue;
// 	}
// 	console.log(arr[i]);
// }

let i = 0;

// while (i <= 10) {
// 	i++;
// 	console.log(i);
// }

do {
	console.log(i);
	i++;
} while (i > 10);
