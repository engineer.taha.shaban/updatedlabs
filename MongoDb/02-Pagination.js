const {default: mongoose} = require("mongoose");
const mangoose = require("mongoose");

mangoose
  .connect("mongodb://localhost/NITA")
  .then(() => console.log(`connection to database done `))
  .catch((err) => console.log(`you have issue =======> ${err}`));

// 2- create schema
const studentSchema = new mongoose.Schema({
  name: String,
  age: Number,
  RegDate: {type: Date, default: Date.now},
  isActive: Boolean,
  //phones: [String],
});

// 3- convert schema to model (class ) you want to insert this data (get / set )
const Student = mongoose.model("Studentdat", studentSchema);

async function createStudentDocument() {
  const student = new Student({
    name: "mohammed mostafa ",
    age: 15,
    isActive: true,
    phones: ["4545454545", "454555445", "455455454"],
  });
  // create
  const result = await student.save();
  console.log(result);
}

// pagination (will show 2 rows in each page )
//api/students?pageNumber=2&pageSize=2
async function getStudent(pageNumber = 1, pageSize = 2) {
  const students = await Student.find()
    .skip((pageNumber - 1) * pageSize) // in first time of loading page will skip 0 
    .limit(pageSize);
  console.log(students);
}
getStudent(7, 2);

