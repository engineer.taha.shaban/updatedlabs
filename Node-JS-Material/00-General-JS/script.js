// variables
let name = "taha saleh";
// JS is case sensitive name not equal to Name
let Name = "taha saleh";
// you cannot use nubers or special char (@-) but you can use _
let _Name = "taha saleh";
// CAMAL Case
let firstName = "taha";
// you can write variable in one word
let lastName = "taha",
  city = "Riyadh",
  sallary = 1500;
// you cannot reasign constant good for testing
const age = 40;
// age = 50;
// console.log(age);
// Data type ===========
// javascript is dynamic programming language but is weekly type
// example in c# : string name = ahmed ;
// you dont need to meention type in JS
// number is equal float and integer
let number = 123;
number = 123.45;
// special numaric values like
console.log(1 / 0); // infinity
console.log("Not Number" / 2.25); // NaN
//Infinity is a number that represents positive infinity.
//-Infinity represents negative infinity.
//A number reaches Infinity when it exceeds the upper limit for a number:
//1.797693134862315E+308.

// BIGINIT more thann this number // ↪ 9007199254740991n
// 'n' at end mean this is BigInt
const bigInt = 9007199254740991343434343434344444434n;

// website to check if JS function supported by browser or not
// https://caniuse.com/?search=bigint

// String we can use it between
// ' ' double quotes or '' Single Quotes or `` backticks
let frstName = "taha";
let lstName = "Saleh";
console.log(frstName + " " + lstName);
console.log(`${frstName}   ${lstName}`);

// Boolean
let nameIsChecked = true; // yes its checked
let ageIsChecked = false; // no its not checked
let isGreater = 4 > 1; // comparison
console.log(isGreater);

// Null
let age2 = null; // mean 'nothing' or 'empety' or 'value unknown '

// undefined
let age3; // value is not assigned
console.log(age);

// symbol type is used to create unique identifier for objects
let id = Symbol();
Symbol("foo") === Symbol("foo"); // false cause each statement take unique identifier
// we use it when we let object generate unique identifier

// typeof
console.log(typeof name);

// comments Windows: Shift + Alt + A; Mac: Shift + Option + A
/* 
let age3; 
console.log(age);
 */

// assignment make varaiable swapping  a=10 , b=5
let a = 5;
let b = 10;
a = a + b; // 5+10 = 15
b = a - b; // 15 - 10 = 5
a = a - b; // 15-5 = 10

// incrementaL operator
var aa = 5;
aa++; // a= a + 1;
aa += 5; // a=a+5;
console.log(aa);

// condentational  statment
// if - else if - switch case
let ageif = 20; // input
// comparison operator
//=== equality
// !==non-equality
//< less than
//> greater than
//
/*
if (expression) {
  // true or false
  //block if true
} else {
  // block if false
}*/
if (ageif === "20") {
  console.log("yes");
} else if (ageif < 10) {
  console.log("age lsss than 10 ");
} else {
  console.log("no");
}

let userName = "taha";
let password = "12345";
if (userName === "taha" && password === "12345") {
  console.log("OK");
} else {
  console.log("invalid user");
}

// switch case
let agesw = 20;
switch (agesw) {
  case 12:
    console.log("age is 12");
    break; // to break incase case matching
  case 20:
    console.log("age is 20");
    break;
  case 30:
    console.log("age is 30");
    break;
  case 40:
    console.log("age is 40");
    break;
  default:
    console.log("No");
    break;
}

// internary operator
let ageint = 20;
let result = age > 20 ? true : false;
console.log(result);

// reference datatype
// object - function - array
let nameref = "Taha Saleh ";
let sallaryref = 1500;
city = "Riyadh";

let person = {
  // better to store it in object
  nameobje: "Taha Saleh",
  sallaryref: 300,
  city: "riyadh",
};
console.log(person.nameobje); // access property
console.log(person["nameobje"]); // access by index
// delete from object
delete person.sallaryref;
console.log(person.sallaryref);

// array
let ageCus1 = 10;
let ageCus2 = 40;
let ageCus3 = 50;
let ageCus4 = 60;
let ageCus = [10, 40, 50, 60];
console.log(ageCus[0]);
console.log(ageCus.length);
console.log((ageCus[0] = 1)); // change item in array
ageCus.pop(); // delete last value from array
ageCus.shift(); // delete from begining
ageCus.unshift(60); // add value from begining
ageCus.push(70); // add value from last of array
ageCus.splice(4, 0, 50); //  from index 4 add value 50
ageCus.splice(3, 1); // at index 3 remoove 1 item
// The splice() method adds and/or removes array elements.
// array.splice(index, howmany, item1, ....., itemX)
// index ==> Required.The position to add/remove items.Negative value defines the position from the end of the array.
// howmany ==> Optional.Number of items to be removed.
// tem1, ..., itemX Optional.New elements(s) to be added.
ageCus.forEach((element) => {
  console.log(element);
});
// index of iteam
console.log(ageCus.indexOf(50));
// Print names in array beside
let namesarr = ["ahmed", "taha", "ali"];
console.log(namesarr.join("-"));

// Spred Operator or REST Syntacs used to make copy of data (syntacs Sugar)
const firstArr = [1, 2, 3];
const secondArr = [...firstArr, 4]; // copy data from first array
console.log(firstArr);
console.log(secondArr);

// copy data of object will remove repeated data will update value with new value
const obj1 = {
  greeting: "Hello",
  nameob: "taha",
  salobj: 500,
};
const obj2 = {
  ...obj1,
  greeting: "Hi",
};

console.log(obj1);
console.log(obj2);

// Loops and iteation
// The while loop loops through a block of code as long as a specified condition is true.
/*while (condition) {
  // code block to be executed
}*/
// program to display numbers from 1 to 5
// initialize the variable
let i = 1,
  n = 5;

// while loop from i = 1 to 5
// while (i <= n) {
//   console.log(`the value is : ${i}`);
//   i++;
// }

// store this values in array
// let arrst = [];
// while (i <= n) {
//   arrst.push(i);
//   console.log(`the value is : ${i}`);
//   i++;
// }
// console.log(arrst);

// store this values in array if condition happen
let arrst = [];
while (i <= n) {
  if (i <= 3) {
    arrst.push(i);
  }
  console.log(`the value is : ${i}`);
  i++;
}
console.log(arrst);

// infinit Loop

// infinit

// different between var and let
for (let row = 0; row <= 7; row++) {
  console.log(row);
}
console.log(row); // var dont respect the scope of function

// There are also other types of loops.
//The for..in loop in JavaScript allows you to iterate over all property keys of an object.



//=======Node Modules =====