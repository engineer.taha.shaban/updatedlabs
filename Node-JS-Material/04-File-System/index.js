var fs = require("fs");
const readme = fs.readFileSync("data.txt", "utf-8");
console.log(readme);

// copy data fromm file to file
fs.writeFileSync("destination.txt", readme);
console.log("=======");
// list all files inside specific directory
const files = fs.readdirSync("./");
files.forEach((ele) => {
  console.log(ele);
});

// read data again
fs.readFile("data.txt", "utf-8", (err, data) => {
  console.log(data);
  // write data in another file (err,wr) is call back function
  fs.writeFile("destination.txt", data, (err, wr) => {});
});
