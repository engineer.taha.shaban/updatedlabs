// we can use yarn or npm to install packages
// to install yarn you can download install file or from cmd
// npm install yarn -g
// moment lib npm i moment
// to generate packages.json you have to use npm init --or-- npm init --yes
var moment = require("moment"); // require
moment().format();
console.log("hello");

// // we can add package under development dependence cause am not going to use it in production
// // try to instal npm i mocha --save-dev used to test code npm i bootstrap --save-prod
// // controll npm version from npm i bootstrap@latest or npm i bootstrap@3.4
// // 3.4.1 ('major' , 'minor','patch-micro')

// // list all packages 
// npm list --global  // list all packages
// npm list --g --depth=0 // list main packages

// // show outdated lib 
// npm outdated
// // update package 
// npm update 

// search library 
// npm search bootstrap

// cache and audit 
// npm cache clean --force
// npm audit

// npx 
// npx -p @angular/cli@9 ng new Aramco --style=css

// publish package you have to register account in npm 
// npm login 
// npm publish 

// yarn 
1- yarn init 
yarn 
yarn add bootstrap // install bootstrap 
yarn remove bootstrap // remove library 
