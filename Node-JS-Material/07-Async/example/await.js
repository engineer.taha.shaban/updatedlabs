// async mean wait data untill retrive
// await mean wait for code untill run

const fetchUsers = async (url) => {
  // wait output of  function
  const res = await fetch(url);
  const data = await res.json();
  console.log(data);
};
const d = fetchUsers("https://jsonplaceholder.typicode.com/users");
