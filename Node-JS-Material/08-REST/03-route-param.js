const express = require("express");
// ===== route parameter
//....../api/courses/15

const app = express();
app.get("/api/courses", (req, res) => {
  res.send(["C3", "html", "JS"]);
});

// // change only take only id
// app.get("/api/courses/:id", (req, res) => {
//   res.send(req.params.id);
// });

// // change only take id and name
// app.get("/api/courses/:id/:name", (req, res) => {
//   res.send(req.params);
// });

// create sort by
// http://localhost:4000/api/courses/6/c#?sortBy=name
app.get("/api/courses/:id/:name", (req, res) => {
  res.send(req.query);
});

app.get("/", (req, res) => {
  res.send("Welcome to my course express taha saleh hhh");
});

const port = process.env.PORT | 4000; // or port 5000
app.listen(port, () => {
  console.log(`listining in port ${port}`);
});
