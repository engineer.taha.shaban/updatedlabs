const express = require("express");

const app = express();

const products = [
  {id: 1, name: "product 1", price: 50},
  {id: 2, name: "product 2", price: 50},
  {id: 3, name: "product 3", price: 50},
  {id: 4, name: "product 4", price: 50},
  {id: 5, name: "product 5", price: 50},
  {id: 6, name: "product 6", price: 50},
  {id: 7, name: "product 7", price: 50},
  {id: 8, name: "product 8", price: 50},
  {id: 9, name: "product 9", price: 50},
  {id: 10, name: "product 10", price: 50},
  {id: 11, name: "product 11", price: 50},
  {id: 9, name: "product 9999", price: 50},
];
app.get("/api/products", (req, res) => {
  res.send(products);
});

// find product from list of products
app.get("/api/products/:pid", (req, res) => {
  //he parseInt() function parses a string argument and returns an integer of the specified
  // first line
  const product = products.filter(
    (element) => element.id === parseInt(req.params.pid)
  );

  // same but am used callback function
  // second line
  // const product = products.find(findId);
  // function findId(element) {
  //   return element.id === parseInt(req.params.pid);
  // }

  // check if product not found
  if (!product) {
    res.status(404).send("the product with given id not found ");
  }
  res.send(product);
});

const port = process.env.PORT | 4000; // or port 5000
app.listen(port, () => {
  console.log(`listining in port ${port}`);
});
