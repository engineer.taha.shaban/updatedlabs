const express = require("express");

const app = express();
// midlle ware to tell server you are going to recive JSON from res
app.use(express.json());

const products = [
  {id: 1, name: "product 1", price: 50},
  {id: 2, name: "product 2", price: 50},
  {id: 3, name: "product 3", price: 50},
  {id: 4, name: "product 4", price: 50},
  {id: 5, name: "product 5", price: 50},
  {id: 6, name: "product 6", price: 50},
  {id: 7, name: "product 7", price: 50},
  {id: 8, name: "product 8", price: 50},
  {id: 9, name: "product 9", price: 50},
  {id: 10, name: "product 10", price: 50},
  {id: 11, name: "product 11", price: 50},
];
app.get("/api/products", (req, res) => {
  const xyz = res.send(products);
  JSON.stringify(xyz);
});

app.post("/api/products", (req, res) => {
  ///******************* validation ********
  //check if less than 3  
  if (!req.body.name || req.body.length < 3) {
    res
      .status(400)
      .send("Name is required and the lenght should be minimum of 3 ");
  }
  // push an object to this array
  const product = {
    id: products.length + 1,
    name: req.body.name, // get name from body of request entered by user
    price: req.body.price,
  };

  products.push(products);
  res.send(product);
});

const port = process.env.PORT | 4000; // or port 5000
app.listen(port, () => {
  console.log(`listining in port ${port}`);
});
