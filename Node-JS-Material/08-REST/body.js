const express = require("express");
const app = express();

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(express.json());

app.post("/todos", (req, res) => {
  console.log(req.body.todo);
});

const port = process.env.PORT | 4000; // or port 5000
app.listen(port, () => {
  console.log(`listining in port ${port}`);
});
