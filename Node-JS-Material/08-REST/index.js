// you have to install express -- npm i express
const express = require("express");

// // you will find post - get - put in express not good way to write it
// express().get;

// so we will add it in const Routing = URL and / mean default of site url
const app = express();
app.get("/api/courses", (req, res) => {
  res.send(["C3", "html", "JS"]);
});

app.get("/", (req, res) => {
  res.send("Welcome to my course express taha saleh hhh");
});

app.listen(4000, () => {
  console.log("listining in port 4000");
});

// to install nodemon npm install -g nodemon # or using yarn: yarn global add nodemon
// nodemon index

// for any host proider you are going to get fixed port and stored in enviroment variable
// how to read it from enviroment variable
const port = process.env.PORT | 4000; // or port 5000
app.listen(port, () => {
  console.log(`listining in port ${port}`);
});


